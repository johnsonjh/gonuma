module github.com/johnsonjh/gonuma

go 1.17

require (
	github.com/intel-go/cpuid v0.0.0-20220614022739-219e067757cb
	github.com/johnsonjh/leaktestfe v0.0.0-20221210113806-1ad56057a826
	github.com/stretchr/testify v1.8.2-0.20221102114659-1333b5d3bda8
	go4.org v0.0.0-20201209231011-d4a079459e60
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/rogpeppe/go-internal v1.9.1-0.20221123163938-fef05454be76 // indirect
	go.uber.org/goleak v1.2.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
